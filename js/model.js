export class ToDoTask {
  constructor(_taskContent, _isCompleted) {
    this.taskContent = _taskContent;
    this.isCompleted = _isCompleted;
  }
}
