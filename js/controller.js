export let renderListTask = (list) => {
  let contentHTML = "";
  list.forEach((task, index) => {
    if (task.isCompleted == false) {
      return (contentHTML += `
            <li>
            ${task.taskContent}
            <div>
            <button onclick="deleteTask(${index})" class="remove"><i class="fa fa-times-circle"></i></button>
            <button onclick="completedTask(${index})" class="complete"><i class="fa fa-check-square"></i></button>
            </div>
            </li>`);
    }
  });
  document.getElementById("todo").innerHTML = contentHTML;
  let contentHTMLCompleted = "";
  list.forEach((task, index) => {
    if (task.isCompleted == true) {
      return (contentHTMLCompleted += `
        <li>
        ${task.taskContent}
        <div>
        <button onclick="deleteTask(${index})" class="remove"><i class="fa fa-times-circle"></i></button>
        </div>
        </li>`);
    }
  });
  document.getElementById("completed").innerHTML = contentHTMLCompleted;
};
