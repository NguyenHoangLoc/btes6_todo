import { renderListTask } from "./controller.js";
import { ToDoTask } from "./model.js";
let listTask = [];
const LISTTASK = "LISTTASK";
let dataTaskJson = localStorage.getItem(LISTTASK);
if (dataTaskJson) {
  listTask = JSON.parse(dataTaskJson);
}
let saveLocalStorage = () => {
  let listTaskJson = JSON.stringify(listTask);
  localStorage.setItem(LISTTASK, listTaskJson);
};
//thêm Task
let addTask = () => {
  let taskContent = document.getElementById("newTask").value;
  if (taskContent == "") return;
  let newTask = new ToDoTask(taskContent, false);
  listTask.push(newTask);
  renderListTask(listTask);
  saveLocalStorage();
  document.getElementById("newTask").value = "";
};
window.addTask = addTask;
//xóa Task
let deleteTask = (index) => {
  listTask.splice(index, 1);
  saveLocalStorage();
  renderListTask(listTask);
};
window.deleteTask = deleteTask;

let completedTask = (index) => {
  listTask[index].isCompleted = true;
  saveLocalStorage();
  renderListTask(listTask);
};
window.completedTask = completedTask;
//sắp xếp A->Z
document.getElementById("az").addEventListener("click", () => {
  listTask = listTask.sort((a, z) => {
    var textA = a.taskContent.toUpperCase();
    var textZ = z.taskContent.toUpperCase();
    return textA < textZ ? -1 : textA > textZ ? 1 : 0;
  });
  renderListTask(listTask);
  saveLocalStorage();
});
//sắp xếp Z->A
document.getElementById("za").addEventListener("click", () => {
  listTask = listTask.sort((a, z) => {
    var textA = a.taskContent.toUpperCase();
    var textZ = z.taskContent.toUpperCase();
    return textA > textZ ? -1 : textA < textZ ? 1 : 0;
  });
  renderListTask(listTask);
  saveLocalStorage();
});
